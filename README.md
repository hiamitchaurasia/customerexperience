# Hashboard Customer Experience

1. Can create and send surveys
2. Initiative Management
3. Complaint Management
4. Can collect data from Hashboard CRM, Project Management, and Finance
5. Customer Profile
6. Design Customer Journey
7. Design User Flow
8. Design Customer Persona

## Running it locally
`mvn spring-boot:run`

