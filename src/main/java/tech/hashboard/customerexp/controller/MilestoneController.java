package tech.hashboard.customerexp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import tech.hashboard.customerexp.entity.MilestoneEntity;
import tech.hashboard.customerexp.service.MilestoneService;

import java.util.List;
import java.util.Optional;

@RequestMapping("/milestones")
@RestController
public class MilestoneController {

	@Autowired
	private MilestoneService service;

	@GetMapping("")
	public List<MilestoneEntity> allMilestones() {
		return service.getAllMilestones();
	}

	@GetMapping("/{id}")
	public Optional<MilestoneEntity> getMilestoneById(@PathVariable("id") Long id) {
		return service.getMilestoneById(id);
	}

	@PostMapping("/create")
	public Long saveMilestone(@RequestBody MilestoneEntity milestone) {
		return service.addMilestone(milestone).getId();
	}

}
