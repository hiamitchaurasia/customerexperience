package tech.hashboard.customerexp.controller;

import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import tech.hashboard.customerexp.dto.InitiativeDto;
import tech.hashboard.customerexp.entity.InitiativeEntity;
import tech.hashboard.customerexp.service.InitiativeService;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@RequestMapping("/initiatives")
@RestController
public class InitiativeController {
	private static final Logger logger = LoggerFactory.getLogger(InitiativeController.class);
	@Autowired
	private InitiativeService service;
	@Autowired
	private ModelMapper modelMapper;

	@GetMapping("")
	public List<InitiativeEntity> allInitiatives() {
		return service.getAllInitiatives();
	}

	@GetMapping("/{id}")
	public Optional<InitiativeEntity> initiativeById(@PathVariable("id") Long id) {
		Optional<InitiativeEntity> initiative = service.getInitiativeById(id);
		logger.info("Fetched Initiative is: " + initiative);
		return initiative;
	}

	@PostMapping("/create")
	public Long saveInitiative(@RequestBody InitiativeDto initiativeDto) {
		InitiativeEntity initiative = modelMapper.map(initiativeDto, InitiativeEntity.class);
		initiative.setCreatedAt(LocalDateTime.now());
		initiative.setUpdatedAt(LocalDateTime.now());
		return service.addInitiative(initiative).getId();
	}

	@PutMapping("/update")
	public Long updateInitiative(@RequestBody InitiativeDto initiativeDto) {
		logger.info("Received InitiativeDto is: " + initiativeDto);
		InitiativeEntity initiative = modelMapper.map(initiativeDto, InitiativeEntity.class);
		initiative.setUpdatedAt(LocalDateTime.now());
		logger.info("Mapped InitiativeEntity is: " + initiative);
		return service.updateInitiative(initiative).getId();
	}
}
