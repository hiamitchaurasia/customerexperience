package tech.hashboard.customerexp.controller;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import tech.hashboard.customerexp.dto.ComplaintTemplateDto;
import tech.hashboard.customerexp.entity.ComplaintTemplateEntity;
import tech.hashboard.customerexp.service.ComplaintTemplateService;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@RequestMapping("/complaintTemplate")
@RestController
public class ComplaintTemplateController {

	@Autowired
	private ComplaintTemplateService service;
	@Autowired
	private ModelMapper modelMapper;

	@GetMapping("")
	public List<ComplaintTemplateEntity> allComplaintTemplates() {
		return service.getAllComplaintTemplates();
	}

	@GetMapping("/{id}")
	public Optional<ComplaintTemplateEntity> getComplaintTemplateById(@PathVariable("id") Long id) {
		return service.getComplaintTemplateById(id);
	}

	@PostMapping("/create")
	public Long createComplaintTemplate(@RequestBody ComplaintTemplateDto complaintTemplateDto) {
		ComplaintTemplateEntity complaintTemplate = modelMapper.map(complaintTemplateDto, ComplaintTemplateEntity.class);
		complaintTemplate.setCreatedAt(LocalDateTime.now());
		complaintTemplate.setUpdatedAt(LocalDateTime.now());
		return service.addComplaintTemplate(complaintTemplate).getId();
	}

	@PutMapping("/update")
	public Long updateComplaintTemplate(@RequestBody ComplaintTemplateDto complaintTemplateDto) {
		ComplaintTemplateEntity complaintTemplate = modelMapper.map(complaintTemplateDto, ComplaintTemplateEntity.class);
		complaintTemplate.setUpdatedAt(LocalDateTime.now());
		return service.addComplaintTemplate(complaintTemplate).getId();
	}
}
