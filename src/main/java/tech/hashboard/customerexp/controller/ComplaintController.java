package tech.hashboard.customerexp.controller;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import tech.hashboard.customerexp.dto.ComplaintDto;
import tech.hashboard.customerexp.entity.ComplaintEntity;
import tech.hashboard.customerexp.service.ComplaintService;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@RequestMapping("/complaints")
@RestController
public class ComplaintController {

	@Autowired
	private ComplaintService service;
	@Autowired
	private ModelMapper modelMapper;
	
	@GetMapping("")
	public List<ComplaintEntity> allComplaints() {
		return service.getAllComplaints();
	}
	
	@GetMapping("/{id}")
	public Optional<ComplaintEntity> getComplaintById(@PathVariable("id") Long id) {
		return service.getComplaintById(id);
	}
	
	@PostMapping("/create")
	public Long saveComplaint(@RequestBody ComplaintDto complaintDto) {
		ComplaintEntity complaint = modelMapper.map(complaintDto, ComplaintEntity.class);
		complaint.setCreatedAt(LocalDateTime.now());
		complaint.setUpdatedAt(LocalDateTime.now());
		return service.addComplaint(complaint).getId();
	}

	@PutMapping("/update")
	public Long updateComplaint(@RequestBody ComplaintDto complaintDto) {
		ComplaintEntity complaint = modelMapper.map(complaintDto, ComplaintEntity.class);
		complaint.setUpdatedAt(LocalDateTime.now());
		return service.addComplaint(complaint).getId();
	}
}
