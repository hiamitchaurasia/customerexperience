package tech.hashboard.customerexp.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.time.LocalDateTime;

@ToString
@Getter
@Setter
public class ComplaintDto {
    private Long id;
    private Long templateId;
    private String status;
    private String customerName;
    private String attachment;
    private String assignedTo;
    private String complaintCategory;
    private String source;
    private String priority;
    private String accountManager;
    private Long projectId;
    private String complaintDescription;
    private String complaintSubject;
    private String complaintSection;
    private String department;
    private String owner;
    private Boolean resolved;
    private Boolean closed;
    private LocalDateTime openedDate;
    private String createdBy;
    private String updatedBy;
}
