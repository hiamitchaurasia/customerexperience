package tech.hashboard.customerexp.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.time.LocalDateTime;

@ToString
@Getter
@Setter
public class InitiativeDto {
    private Long id;
    private String initiativeName;
    private String objective;
    private String owner;
    private String type;
    private String priority;
    private Integer durationInDays;
    private Integer completionPercentage;
    private String members;
    private String comments;
    private String attachment;
    private Boolean done;
    private Boolean closed;
    private LocalDateTime startDate;
    private LocalDateTime endDate;
    private String createdBy;
    private String updatedBy;
}
