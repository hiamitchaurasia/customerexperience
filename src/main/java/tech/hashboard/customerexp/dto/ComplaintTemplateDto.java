package tech.hashboard.customerexp.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.time.LocalDateTime;

@ToString
@Getter
@Setter
public class ComplaintTemplateDto {
    private Long id;
    private String templateName;
    private String summary;
    private String details;
    private Boolean active;
    private String createdBy;
    private String updatedBy;
}
