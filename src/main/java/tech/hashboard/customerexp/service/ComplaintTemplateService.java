package tech.hashboard.customerexp.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tech.hashboard.customerexp.entity.ComplaintTemplateEntity;
import tech.hashboard.customerexp.entity.MilestoneEntity;
import tech.hashboard.customerexp.repository.ComplaintTemplateRepository;

import java.util.List;
import java.util.Optional;

@Service
public class ComplaintTemplateService {
	@Autowired
	private ComplaintTemplateRepository repo;

	public ComplaintTemplateEntity addComplaintTemplate(ComplaintTemplateEntity complaintTemplate) {
		return repo.save(complaintTemplate);
	}

	public List<ComplaintTemplateEntity> editComplaintTemplate() {
		List<ComplaintTemplateEntity> complaintTemplates = repo.findAll();
		return complaintTemplates;
	}

	public List<ComplaintTemplateEntity> getAllComplaintTemplates() {
		List<ComplaintTemplateEntity> complaintTemplates = repo.findAll();
		return complaintTemplates;
	}

	public Optional<ComplaintTemplateEntity> getComplaintTemplateById(Long id) {
		Optional<ComplaintTemplateEntity> complaintTemplate = repo.findById(id);
		return complaintTemplate;
	}

	public Long deleteMilestone(Long id) {
		repo.deleteById(id);
		return id;
	}
}
