package tech.hashboard.customerexp.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tech.hashboard.customerexp.entity.InitiativeEntity;
import tech.hashboard.customerexp.entity.MilestoneEntity;
import tech.hashboard.customerexp.repository.InitiativeRepository;

import java.util.List;
import java.util.Optional;

@Service
public class InitiativeService {

	@Autowired
	private InitiativeRepository repo;

	public InitiativeEntity addInitiative(InitiativeEntity initiative) {
		return repo.save(initiative);
	}

	public InitiativeEntity updateInitiative(InitiativeEntity initiative) {
		Optional<InitiativeEntity> initiativeEntity = repo.findById(initiative.getId());
		if(initiativeEntity.isPresent()) {
			InitiativeEntity initiativeEntity1 = initiativeEntity.get();
			initiativeEntity1.setInitiativeName(initiative.getInitiativeName());
			initiativeEntity1.setAttachment(initiative.getAttachment());
			initiativeEntity1.setClosed(initiative.getClosed());
			initiativeEntity1.setComments(initiative.getComments());
			initiativeEntity1.setCompletionPercentage(initiative.getCompletionPercentage());
			initiativeEntity1.setCreatedAt(initiative.getCreatedAt());
			initiativeEntity1.setCreatedBy(initiative.getCreatedBy());
			initiativeEntity1.setDone(initiative.getDone());
			initiativeEntity1.setDurationInDays(initiative.getDurationInDays());
			initiativeEntity1.setEndDate(initiative.getEndDate());
			initiativeEntity1.setMembers(initiative.getMembers());
			initiativeEntity1.setObjective(initiative.getObjective());
			initiativeEntity1.setOwner(initiative.getOwner());
			initiativeEntity1.setPriority(initiative.getPriority());
			initiativeEntity1.setStartDate(initiative.getStartDate());
			initiativeEntity1.setType(initiative.getType());
			initiativeEntity1.setUpdatedAt(initiative.getUpdatedAt());
			initiativeEntity1.setUpdatedBy(initiative.getUpdatedBy());
			repo.save(initiativeEntity1);
			return initiativeEntity1;
		}
		return null;
	}

	public List<InitiativeEntity> getAllInitiatives() {
		List<InitiativeEntity> initiatives = repo.findAll();
		return initiatives;
	}

	public Optional<InitiativeEntity> getInitiativeById(Long id) {
		Optional<InitiativeEntity> initiative = repo.findById(id);
		return initiative;
	}

	public Long deleteInitiative(Long id) {
		repo.deleteById(id);
		return id;
	}
}
