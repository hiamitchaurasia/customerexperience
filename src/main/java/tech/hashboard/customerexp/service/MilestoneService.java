package tech.hashboard.customerexp.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tech.hashboard.customerexp.entity.MilestoneEntity;
import tech.hashboard.customerexp.repository.MilestoneRepository;

import java.util.List;
import java.util.Optional;

@Service
public class MilestoneService {

	@Autowired
	private MilestoneRepository repo;

	public MilestoneEntity addMilestone(MilestoneEntity milestone) {
		return repo.save(milestone);
	}

	public List<MilestoneEntity> editMilestone() {
		List<MilestoneEntity> milestones = repo.findAll();
		return milestones;
	}

	public List<MilestoneEntity> getAllMilestones() {
		List<MilestoneEntity> milestones = repo.findAll();
		return milestones;
	}

	public Optional<MilestoneEntity> getMilestoneById(Long id) {
		Optional<MilestoneEntity> milestones = repo.findById(id);
		return milestones;
	}

	public Long deleteMilestone(Long id) {
		repo.deleteById(id);
		return id;
	}

}
