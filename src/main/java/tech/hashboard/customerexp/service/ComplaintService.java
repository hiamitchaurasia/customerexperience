package tech.hashboard.customerexp.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tech.hashboard.customerexp.entity.ComplaintEntity;
import tech.hashboard.customerexp.entity.InitiativeEntity;
import tech.hashboard.customerexp.repository.ComplaintRepository;

import java.util.List;
import java.util.Optional;

@Service
public class ComplaintService {
	@Autowired
	private ComplaintRepository repo;

	public ComplaintEntity addComplaint(ComplaintEntity complaint) {
		return repo.save(complaint);
	}

	public List<ComplaintEntity> editComplaint() {
		List<ComplaintEntity> complaints = repo.findAll();
		return complaints;
	}

	public List<ComplaintEntity> getAllComplaints() {
		List<ComplaintEntity> complaints = repo.findAll();
		return complaints;
	}

	public Long closeComplaint(Long id) {
		repo.closeComplaint(id);
		return id;
	}

	public Long deleteComplaint(Long id) {
		repo.deleteById(id);
		return id;
	}

	public Optional<ComplaintEntity> getComplaintById(Long id) {
		Optional<ComplaintEntity> complaint = repo.findById(id);
		return complaint;
	}

}
