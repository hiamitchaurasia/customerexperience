package tech.hashboard.customerexp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import tech.hashboard.customerexp.entity.MilestoneEntity;

public interface MilestoneRepository extends JpaRepository<MilestoneEntity, Long> {
}
