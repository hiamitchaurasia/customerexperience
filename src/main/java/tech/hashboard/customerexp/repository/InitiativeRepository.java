package tech.hashboard.customerexp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import tech.hashboard.customerexp.entity.InitiativeEntity;

public interface InitiativeRepository extends JpaRepository<InitiativeEntity, Long> {
}
