package tech.hashboard.customerexp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import tech.hashboard.customerexp.entity.ComplaintEntity;

public interface ComplaintRepository extends JpaRepository<ComplaintEntity, Long> {
    @Modifying
    @Query("UPDATE ComplaintEntity r SET r.closed = false WHERE r.id = ?1")
    public void closeComplaint(Long id);
}
