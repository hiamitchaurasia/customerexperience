package tech.hashboard.customerexp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import tech.hashboard.customerexp.entity.ComplaintTemplateEntity;

public interface ComplaintTemplateRepository extends JpaRepository<ComplaintTemplateEntity, Long> {
}
