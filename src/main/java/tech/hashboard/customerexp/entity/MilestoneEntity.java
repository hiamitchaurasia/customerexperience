package tech.hashboard.customerexp.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.time.LocalDateTime;

@ToString
@Getter
@Setter
@Entity
@Table(name = "milestones")
public class MilestoneEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "is_done")
    private Boolean done;
    @Column(name = "initiative_id")
    private Long initiativeId;
    @Column(name = "milestone_name")
    private String milestoneName;
    @Column(name = "milestone_description")
    private String description;
    @Column(name = "owner")
    private String owner;
    @Column(name = "type")
    private String type;
    @Column(name = "milestone_priority")
    private String priority;
    @Column(name = "duration_in_days")
    private Integer durationInDays;
    @Column(name = "completion_percentage")
    private Integer completionPercentage;
    @Column(name = "milestone_kpi")
    private String milestoneKpi;
    @Column(name = "milestone_members")
    private String members;
    @Column(name = "created_by")
    private String createdBy;
    @Column(name = "updated_by")
    private String updatedBy;
    @Column(name = "created_at")
    private LocalDateTime createdAt;
    @Column(name = "updated_at")
    private LocalDateTime updatedAt;
}
