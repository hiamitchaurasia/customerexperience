package tech.hashboard.customerexp.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.time.LocalDateTime;

@ToString
@Getter
@Setter
@Entity
@Table(name = "initiatives")
public class InitiativeEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "initiative_name")
    private String initiativeName;
    @Column(name = "objective")
    private String objective;
    @Column(name = "owner")
    private String owner;
    @Column(name = "type")
    private String type;
    @Column(name = "initiative_priority")
    private String priority;
    @Column(name = "duration_in_days")
    private Integer durationInDays;
    @Column(name = "completion_percentage")
    private Integer completionPercentage;
    @Column(name = "initiative_members")
    private String members;
    @Column(name = "comments")
    private String comments;
    @Column(name = "attachment")
    private String attachment;
    @Column(name = "is_done")
    private Boolean done;
    @Column(name = "is_closed")
    private Boolean closed;
    @Column(name = "start_date")
    private LocalDateTime startDate;
    @Column(name = "end_date")
    private LocalDateTime endDate;
    @Column(name = "created_by")
    private String createdBy;
    @Column(name = "updated_by")
    private String updatedBy;
    @Column(name = "created_at")
    private LocalDateTime createdAt;
    @Column(name = "updated_at")
    private LocalDateTime updatedAt;
}
