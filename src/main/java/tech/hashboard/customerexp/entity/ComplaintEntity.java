package tech.hashboard.customerexp.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.time.LocalDateTime;

@ToString
@Getter
@Setter
@Entity
@Table(name = "complaints")
public class ComplaintEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "template_id")
    private Long templateId;
    @Column(name = "status")
    private String status;
    @Column(name = "customer_name")
    private String customerName;
    @Column(name = "attachment")
    private String attachment;
    @Column(name = "assigned_to")
    private String assignedTo;
    @Column(name = "complaint_category")
    private String complaintCategory;
    @Column(name = "source")
    private String source;
    @Column(name = "priority")
    private String priority;
    @Column(name = "account_manager")
    private String accountManager;
    @Column(name = "project_id")
    private Long projectId;
    @Column(name = "complaint_description")
    private String complaintDescription;
    @Column(name = "complaint_subject")
    private String complaintSubject;
    @Column(name = "complaint_section")
    private String complaintSection;
    @Column(name = "department")
    private String department;
    @Column(name = "owner")
    private String owner;
    @Column(name = "is_resolved")
    private Boolean resolved;
    @Column(name = "is_closed")
    private Boolean closed;
    @Column(name = "opened_date")
    private LocalDateTime openedDate;
    @Column(name = "created_by")
    private String createdBy;
    @Column(name = "updated_by")
    private String updatedBy;
    @Column(name = "created_at")
    private LocalDateTime createdAt;
    @Column(name = "updated_at")
    private LocalDateTime updatedAt;
}
